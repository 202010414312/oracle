﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414312    姓名：聂云飞

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 1.创建表空间

```
-- 创建DATA表空间
CREATE TABLESPACE data
DATAFILE DATA.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 10M
MAXSIZE UNLIMITED;​

-- 创建 index_space表空间
CREATE TABLESPACE index_space
DATAFILE index_space.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 10M
MAXSIZE UNLIMITED;

```
![1.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/1.png?inline=false)

#### 2.创建表
```
-- 创建Product表
CREATE TABLE Product (
    product_id NUMBER PRIMARY KEY, 
    product_name VARCHAR2(50), 
    product_desc VARCHAR2(100), 
    price NUMBER(10,2)
) TABLESPACE DATA;

TABLESPACE sales_data;
```
![2.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/2.png?inline=false)

```
CREATE TABLE Order (
    order_id NUMBER PRIMARY KEY, 
    customer_name VARCHAR2(50),
    order_time TIMESTAMP, 
    CONSTRAINT fk_customer FOREIGN KEY (customer_name)
        REFERENCES Customer(customer_name)
) TABLESPACE DATA;
```  

![3.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/3.png?inline=false)
 

```
-- 创建 Order_Detail表
CREATE TABLE Order_Detail (
    order_detail_id NUMBER PRIMARY KEY, 
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER,
    CONSTRAINT fk_order FOREIGN KEY (order_id)
        REFERENCES Order(order_id),
    CONSTRAINT fk_product FOREIGN KEY (product_id)
        REFERENCES Product(product_id)
) TABLESPACE DATA;
```

![4.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/4.png?inline=false)  

```
-- 创建Customer表
CREATE TABLE Customer (
    customer_name VARCHAR2(50) PRIMARY KEY, 
    address VARCHAR2(100), 
    contact_info VARCHAR2(20)
) TABLESPACE DATA;
```

![5.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/5.png?inline=false)    
 
 ```
```   

#### 3.插入数据
```
商品表

--为商品表插入10w条数据
BEGIN
  FOR i IN 1..100000 LOOP
      INSERT INTO Product (product_id,product_ name,product_desc price)
      VALUES (i, 'Product ' || i, ‘Description’|| i, ROUND(DBMS-RANDOM.VALUE(1,1000),2));     
END LOOP;
COMMIT;
DBMS-OUTPUT.PUT_LINE(‘Product 表数据插入完成。’);
END;
```

![6.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/6.png?inline=false)

顾客表

```
-- 为客户表插入5w数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO TB_CUSTOMER (id, customer_no, name,phone,address)
    VALUES (i, 'cust('100',i)','张三 ' || i, FLOOR(dbms_random.value(1000000000,20000000000),concat('xxx地址00',i));
  END LOOP;
  COMMIT;
END
```

![7.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/7.png?inline=false)

订单表

```
--为订单表插入10w数据
INSERT INTO TB_ORDER1 (order_id, product_id, quantity, price)
SELECT level, mod(level, 1000)+1, mod(level, 10)+1, mod(level, 100)+1
FROM dual
CONNECT BY level <= 20000;
```

![8.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/8.png?inline=false)

#### 4.设计权限及用户分配

```
--在数据库中创建两个用户：
- ADMIN：拥有完全的DBA权限，包括创建数据库和表、分配权限等。
- USER：用于管理商品和订单等业务相关的操作，包括添加、修改或删除产品、订单等。
CREATE USER ADMIN IDENTIFIED BY password;
GRANT CONNECT, DBA, RESOURCE TO ADMIN;

CREATE USER USER IDENTIFIED BY password;
GRANT CONNECT, RESOURCE TO USER;

-- USER用户只拥有Product和Order_Detail表的读取权限
GRANT SELECT ON Product TO USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON Order_Detail TO USER;
```

![9.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/9.png?inline=false)  
![10.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/10.png?inline=false)

#### 5.PL/SQL设计 
--程序包、存储过程和函数

CREATE OR REPLACE PACKAGE Order_Pkg AS

    -- 创建新订单的存储过程
    PROCEDURE Create_Order(
        p_customer_name IN VARCHAR2,
        p_product_id IN NUMBER,
        p_quantity IN NUMBER    
    );

END Order_Pkg;
/

CREATE OR REPLACE PACKAGE BODY Order_Pkg AS

    PROCEDURE Create_Order(
        p_customer_name IN VARCHAR2,
        p_product_id IN NUMBER,
        p_quantity IN NUMBER
    ) AS
        l_order_id NUMBER;
    BEGIN
        -- 生成新的订单号
        SELECT MAX(order_id) + 1 INTO l_order_id FROM Order;
        
        -- 在订单表中插入新的订单信息
        INSERT INTO Order(order_id, customer_name, order_time)
        VALUES(l_order_id, p_customer_name, SYSTIMESTAMP);
        
        -- 在订单详情表中插入订单的详细信息
        INSERT INTO Order_Detail(order_detail_id, order_id, product_id, quantity)
        VALUES(l_order_id, p_product_id, p_quantity);
        
        COMMIT;
    END Create_Order;

END Order_Pkg;
```
```
![11.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/11.png?inline=false)
![12.png](https://gitlab.com/202010414312/oracle/-/raw/main/test6/picture/12.png?inline=false)

```
```
#### 6.备份方案

> 以下是一个包含核心代码的基本数据库备份方案
>
> 1. 创建备份目录，用于存储备份和恢复文件：
>
> 2. 创建备份脚本backup.sh
> #!/bin/bash
    rman target /
    run {
      allocate channel c1 device type disk format '/u01/backup/backupset_%U';
      backup as backupset database plus archivelog;
}  
>
> 3. 定期备份策略：
>    使用crontab将备份脚本定期执行，例如每天晚上10点：
>
> 4.  将备份数据拷贝到备份目录中，并定期进行测试性的恢复操作
>    - 每周进行一次全量备份，保留最近30天的备份文件。
>    - 在备份脚本中使用`find`命令，删除30天前的备份文件。
>
> 5. 针对备份目录中的备份数据执行后续的恢复操作：
>    rman target /
        run {
            set until time '2021-11-01 12:00:00';
            restore database;
            recover database;
}

### 总结

实验分析与总结：

在设计基于Oracle数据库的商品销售系统的数据库方案时，我进行了以下步骤：

1. 表空间设计：创建了两个表空间 DATA：用于存储数据。INDEX：用于存放索引。以提高查询性能并有效管理数据库存储空间。

2. 表设计：设计了四张表，在数据库中创建四张表：商品表（Product）：存储所有商品的信息，包括名称、描述、价格等。 订单表（Order）：存储所有订单的信息，包括订单号、客户名称、下单时间等。订单详情表（Order_Detail）：存储每个订单的详细信息，包括订单号、商品信息、数量等。客户表（Customer）：存储所有客户的信息，包括名称、地址、联系方式等。形成了数据库的基本数据结构。

3. 用户和权限分配：创建了两个用户，ADMIN：拥有完全的DBA权限，包括创建数据库和表、分配权限等。USER：用于管理商品和订单等业务相关的操作，包括添加、修改或删除产品、订单等。

4. 程序包设计：创建了一个名为Order_pkg的程序包，封装了一些存储过程和函数，实现了复杂的业务逻辑。在数据库中创建一个程序包（Package），该程序包包含一些存储过程和函数，用于实现比较复杂的业务逻辑。这些存储过程和函数可以方便地被调用，提供了对订单、商品和客户等数据的处理和查询功能。。

5. 数据库备份方案：提出了一套数据库备份方案，包括定期全量备份、测试备份的可恢复性。这样可以保证数据库数据的安全性和可靠性，以及在灾难发生时能够快速恢复数据库。   

通过实验分析，得出以下结论：

1. 通过将数据和索引分开存储，可以避免数据碎片和提升查询速度。合理的表空间设计和分配可以提高数据库的性能和管理效率。

2. 可以通过SQL脚本或Oracle SQL Developer等工具实现设计模型，如创建表、插入数据、创建索引等操作。

3. 程序包的使用可以简化复杂业务逻辑的实现。将相关的存储过程和函数封装在程序包中，可以提高代码的可维护性和重用性。

4. 数据库备份是数据库管理的重要环节。合理的备份策略和方案可以保证数据的完整性和可恢复性，防止数据丢失和灾难发生时的数据库故障。了解Oracle提供的安全功能，例如用户管理、角色和权限、加密数据、审计等，以及实施与数据库安全相关的最佳实践。

通过本次亲自实现一个完整的项目，对oracle数据库的知识进行了深层次的学习和掌握，通过本次项目，对表设计，表空间设计，程序包设计，数据库的备份从懵懂到掌握，再到亲自实现，让我对oracle的知识的掌握，项目的实现的方面有了很大的收获，让未来我可以去接触和解决oracle相关问题，不仅对我学习上具有很大帮助，在未来的就业上也受益匪浅。
