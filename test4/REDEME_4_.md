# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414312 姓名：聂云飞 班级：2020级软工3班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

```sql
create or replace procedure YHTriange(N in integer) 
is
   type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
   -- N integer := 9; -- 一共打印9行数字
    rowArray t_number := t_number();
begin
   dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
end YHTriange;
```

调用存储过程：

```sql

set serveroutput on;
declare
   begin
      YHTriange(10); 
   end;
```

截图：
![pict1](https://gitlab.com/202010414312/oracle/-/raw/main/test4/pict1.png?inline=false)运行结果

## 实验总结

Oracle PL/SQL是一种编程语言，用于编写存储过程和触发器等数据库对象。它是Oracle数据库的一部分，可以帮助开发人员更好地管理和处理数据。
  
  学习基本的PL/SQL语法，如变量、控制结构、函数和过程等。  


熟悉Oracle数据库的基本概念和SQL语法，包括表、视图、索引和触发器等。  


理解存储过程的概念和用途，学习如何编写存储过程，包括输入参数、输出参数和异常处理等。  


学习如何优化存储过程的性能，包括使用游标、批量操作和合理的查询语句等。  


熟悉Oracle PL/SQL的调试工具和技巧，包括使用调试器和日志记录等  
