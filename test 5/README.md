# 实验5：包，过程，函数的用法
**姓名：聂云飞**
**学号：202010414312**
## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); 
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![](step1.png)

*这段代码是创建一个名为 "MyPack" 的包，其中包含一个名为 "Get_SalaryAmount" 的函数和一个名为 "Get_Employees" 的过程。  *  

*"Get_SalaryAmount" 函数接受一个部门 ID，返回该部门的所有员工薪资总和。    *  

*"Get_Employees" 过程接受一个员工 ID，递归查询该员工及其下属，并以树形结构输出。*

*注意事项：在包中可以声明全局变量，供包体中的多个子程序使用，但要注意控制变量的作用域和声明的类型。此外，在包体中使用DBMS_OUTPUT.PUT_LINE可以在SQL Developer或PL/SQL Developer中的输出窗口显示调试信息。*


## 测试

```sql
函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

```
![](step2.png)

*这是一个使用自定义函数Get_SalaryAmount()计算部门工资总额的SQL查询语句：*

*其中，MyPack是一个自定义的包，Get_SalaryAmount()是该包中的一个函数，用于计算指定部门的工资总额。该查询语句将返回每个部门的ID、名称和工资总额。*

```sql
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```
![](step3.png)
*这段代码是一个 PL/SQL 过程，它定义了一个名为 V_EMPLOYEE_ID 的变量，类型为 NUMBER。然后，在 BEGIN 和 END 关键字之间，可以编写一些 SQL 语句来执行一些操作，例如查询数据库中的员工信息，并将结果存储在变量中。*

*代码块的第一行设置了服务器输出(serveroutput)参数，以便在SQL Developer或PL/SQL Developer的输出窗口中显示调试信息。*

*接下来，使用DECLARE关键字声明要使用的变量V_EMPLOYEE_ID，其数据类型为NUMBER。*

*在BEGIN...END语句块中，将V_EMPLOYEE_ID的初始值设置为101，然后调用包MyPack中的Get_Employees过程，并将V_EMPLOYEE_ID作为参数传递给这个过程。*

*set serveroutput on 是用来开启 PL/SQL 的输出功能，可以通过 DBMS_OUTPUT.PUT_LINE() 函数将一些信息输出到控制台。*

- 使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工:

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
![](step4.png)

*这段 SQL 语句的作用是从 employees 表中选取 LEVEL、EMPLOYEE_ID、FIRST_NAME 和 MANAGER_ID 四个字段，其中 START WITH EMPLOYEE_ID = V_EMP 表示从 EMPLOYEE_ID 等于 V_EMP 的记录开始，然后使用递归查询获取该员工的所有下属员工及其下属员工。*

*具体而言，通过START WITH子句指定查询的起始点，即EMPLOYEE_ID等于V_EMPLOYEE_ID的员工；通过CONNECT BY子句指定查询的递归条件，即每个员工的EMPLOYEE_ID与其上级领导的MANAGER_ID相连通。*

*查询结果包括四列数据：LEVEL表示递归的级别，而EMPLOYEE_ID、FIRST_NAME和MANAGER_ID分别代表员工编号、姓名和直接上级的编号。*  
*如果 V_EMP 是一个变量，那么该语句将根据不同的 V_EMP 值返回不同的结果。*


## 实验总结
1. 本次实验的主要目的是让学生能够熟练掌握PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程和函数的用法。

2. 在本次实验中，我们使用Oracle数据库和SQL Developer工具进行编码和测试。实验包含了创建包和其内部的函数和过程，同时使用游标、递归查询等功能，实现了查询每个部门的工资总额和树形结构输出员工信息的功能。

3. 通过本次实验，我们能够了解如何使用PL/SQL来实现复杂的数据处理功能，并且加深对Oracle数据库的理解和掌握。

4. 在实验过程中，我们需要注意变量的作用域和声明的类型，正确使用PL/SQL语言结构和关键字，并且保持良好的编码风格和正确的注释。

5. PL/SQL是一种过程化编程语言，它是Oracle数据库的一部分，用于编写存储过程、触发器、函数和包。  

6. 变量的声明：

DECLARE variable_name datatype [NOT NULL := value]; BEGIN -- code block END;

常量的声明：

DECLARE constant_name CONSTANT datatype := value; BEGIN -- code block END;

变量和常量的使用：

在PL/SQL中，变量和常量可以用于存储数据和进行计算。以下是一些常见的用法：

-- 声明变量和常量 DECLARE v_num1 NUMBER := 10; v_num2 NUMBER := 20; v_result NUMBER; c_num3 CONSTANT NUMBER := 30; BEGIN -- 计算 v_result := v_num1 + v_num2 + c_num3; -- 输出结果 DBMS_OUTPUT.PUT_LINE('Result: ' || v_result); END;  

7. 学习包（Package）是一种组织 Python 模块的方法，使得模块更易于重用和共享。学习包的过程是将多个相关的模块组织在一个文件夹中，并在文件夹中创建一个名为 init.py 的文件来指示 Python 该文件夹是一个包。

函数（Function）是一种可重复使用的代码块，它接受输入、执行操作并返回结果。函数的用法是定义一个函数并传递参数，然后调用该函数并使用返回值。